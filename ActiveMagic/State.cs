﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public struct State : IRenameable
    {
        public string Name { get; private set; }

        public Range Condition { get; private set; }

        public bool IsIncluded { get; private set; }

        public event EventHandler ActivatedState;

        public event EventHandler DisactivatedState;

        public State(string name, Range condition, bool isIncluded, EventHandler activatedState, EventHandler disactivatedState)
        {
            Name = name;
            Condition = condition;
            IsIncluded = isIncluded;
            ActivatedState = activatedState;
            DisactivatedState = disactivatedState;
        }

        public void ActivateState() => ActivatedState?.Invoke(this, new EventArgs());

        public void DisactivateState() => DisactivatedState?.Invoke(this, new EventArgs());

        public void ChangeCondition(Range condition) => Condition = condition;

        public void Rename(string newName) => Name = newName;

        public void SetIncluded(bool isIncluded) => new State(Name, Condition, isIncluded, ActivatedState, DisactivatedState);

        public override string ToString()
        {
            return $"Название состояния: {Name}\nУсловие возникновения состояния: {Condition}\nДоступность состояния: {IsIncluded}\n";
        }
    }
}
