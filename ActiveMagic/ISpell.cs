﻿using System;

namespace ActiveMagic
{
    public enum SchoolMagic { Shine, Dark, Chaos }

    public enum TargetMagic { Myself, Friend, Enemy, Region, All }

    public interface ISpell
    {
        SchoolMagic School { get; }

        string Name { get; }

        int CountMana { get; }

        TargetMagic Target { get; }

        ICharacteristic<Type> TargetCharacteristic { get; }

        decimal PowerUnit { get; }

        public void Choose();

        public void Enhance();

        public void Read();

        public void Apply();

        public void Undo();
    }
}