﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public interface IStateController
    {
        StateCollection States { get; }

        void AddState(State state);

        void ChangeState(State state);

        void RemoveState(State state);
    }
}
