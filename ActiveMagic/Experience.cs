﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public class Experience
    {
        private uint current;

        public string Name { get; } = "Experience";

        public uint Current { get => current; private set { if (value > Maximum) current = Maximum; else if (value < Minimum) current = Minimum; else current = value; } }

        public uint Minimum { get; } = 0;

        public uint Maximum { get; }

        public uint DefaultValue => throw new NotImplementedException();

        public event EventHandler Changed;

        public event EventHandler Increased;

        public event EventHandler Decreased;
        public event EventHandler ChangedMinimum;
        public event EventHandler Minimized;
        public event EventHandler ChangedMaximum;
        public event EventHandler Maximized;
        public event EventHandler ChangedDefaultValue;
        public event EventHandler RestoredDefaultValue;

        public Experience(uint maximum, uint current, uint minimum, string name)
        {
            Maximum = maximum;
            Minimum = minimum;
            Current = current;
            Name = name;
        }

        public Experience(uint maximum, uint current, uint minimum)
        {
            Maximum = maximum;
            Minimum = minimum;
            Current = current;
        }

        public Experience(uint maximum, uint current)
        {
            Maximum = maximum;
            Current = current;
        }

        public Experience(uint maximum)
        {
            Maximum = maximum;
            Current = maximum;
        }

        public void Decrease(uint dicrement)
        {
            Current -= dicrement;
            Changed?.Invoke(this, new EventArgs());
            Decreased?.Invoke(this, new EventArgs());
        }

        public void Increase(uint increment)
        {
            Current += increment;
            Changed?.Invoke(this, new EventArgs());
            Increased?.Invoke(this, new EventArgs());
        }

        public void Change(uint newCurrent)
        {
            throw new NotImplementedException();
        }

        public void ChangeMinimum(uint newMinimum)
        {
            throw new NotImplementedException();
        }

        public void ResetToMinimum()
        {
            throw new NotImplementedException();
        }

        public void ChangeMaximum(uint newMaximum)
        {
            throw new NotImplementedException();
        }

        public void ResetToMaximum()
        {
            throw new NotImplementedException();
        }

        public void ChangeDefaultValue(uint newDefault)
        {
            throw new NotImplementedException();
        }

        public void RestoreToDefaultValue()
        {
            throw new NotImplementedException();
        }
    }
}
