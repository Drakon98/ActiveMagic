﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public class Mana : INameable
    {
        private uint current;

        public string Name { get; }

        public uint Current { get => current; private set { if (value > Maximum) current = Maximum; else if (value >= Minimum) current = value; } }

        public uint Minimum { get; private set; } = 0;

        public uint Maximum { get; private set; }

        public int Regeneration { get; private set; }

        public uint DefaultValue => throw new NotImplementedException();

        public event EventHandler Changed;

        public event EventHandler ChangedMaximum;

        public event EventHandler ChangedMinimum;

        public event EventHandler ChangedRegenerate;

        public event EventHandler Decreased;

        public event EventHandler Increased;
        
        public event EventHandler Minimized;
        
        public event EventHandler Maximized;
        
        public event EventHandler Regenerated;
        public event EventHandler ChangedDefaultValue;
        public event EventHandler RestoredDefaultValue;

        public Mana(uint maximum = 100, uint current = 50, uint minimum = 0, int regeneration = 0, string name = "Mana")
        {
            Maximum = maximum;
            Minimum = minimum;
            Current = current;
            Regeneration = regeneration;
            Name = name;
        }

        public Mana(uint maximum = 100, uint minimum = 0, int regeneration = 0, string name = "Mana")
        {
            Maximum = maximum;
            Minimum = minimum;
            Current = maximum;
            Regeneration = regeneration;
            Name = name;
        }

        public void Change(uint newCurrent)
        {
            Current = newCurrent;
            Changed?.Invoke(this, new EventArgs());
        }

        public void ChangeMaximum(uint newMaximum)
        {
            Maximum = newMaximum;
            ChangedMaximum?.Invoke(this, new EventArgs());
        }

        public void ChangeMinimum(uint newMinimum)
        {
            Minimum = newMinimum;
            ChangedMinimum?.Invoke(this, new EventArgs());
        }

        public void ChangeRegenerate(int newRegeneration)
        {
            Regeneration = newRegeneration;
            ChangedRegenerate?.Invoke(this, new EventArgs());
        }

        public void Decrease(uint dicrement)
        {
            Current -= dicrement;
            Decreased?.Invoke(this, new EventArgs());
        }

        public void Increase(uint increment)
        {
            Current += increment;
            Increased?.Invoke(this, new EventArgs());
        }

        public void ResetToMinimum()
        {
            Current = Minimum;
            Minimized?.Invoke(this, new EventArgs());
        }

        public void ResetToMaximum()
        {
            Current = Maximum;
            Maximized?.Invoke(this, new EventArgs());
        }

        public void Regenerate()
        {
            if (Current < Maximum)
            {
                Current += (uint)Regeneration;
                Regenerated?.Invoke(this, new EventArgs());
            }
        }

        public void ChangeDefaultValue(uint newDefault)
        {
            throw new NotImplementedException();
        }

        public void RestoreToDefaultValue()
        {
            throw new NotImplementedException();
        }
    }
}
