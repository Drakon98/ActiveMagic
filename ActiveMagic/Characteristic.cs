﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public class Characteristic<T> : ICharacteristic<T> where T : struct, IComparable
    {
        private T minimum;

        private T current;

        private T maximum;

        private T defaultValue;

        public T Minimum
        {
            get => minimum;

            set
            {
                if (value.CompareTo(current) > 0)
                {
                    current = value;
                    Changed?.Invoke(this, new EventArgs());
                }
                if (value.CompareTo(defaultValue) > 0)
                {
                    defaultValue = value;
                    ChangedDefaultValue?.Invoke(this, new EventArgs());
                }
                minimum = value;
                ChangedMinimum?.Invoke(this, new EventArgs());
            }
        }

        public T Current
        {
            get => current;

            set
            {
                if (value.CompareTo(Maximum) > 0)
                {
                    Console.WriteLine($"Value: {value}");
                    current = Maximum;
                }
                else if (value.CompareTo(Minimum) < 0)
                {
                    Console.WriteLine($"Value: {value}");
                    current = Minimum;
                }
                else
                {
                    current = value;
                }
                Changed?.Invoke(this, new EventArgs());
            }
        }

        public T Maximum
        {
            get => maximum;

            set
            {
                if (value.CompareTo(current) < 0)
                {
                    current = value;
                    Changed?.Invoke(this, new EventArgs());
                }
                else if (value.CompareTo(defaultValue) < 0)
                {
                    defaultValue = value;
                    ChangedDefaultValue?.Invoke(this, new EventArgs());
                }
                maximum = value;
                ChangedMaximum?.Invoke(this, new EventArgs());
            }
        }

        public T DefaultValue
        {
            get => defaultValue;

            set
            {
                if (value.CompareTo(Maximum) > 0)
                {
                    defaultValue = Maximum;
                }
                else if (value.CompareTo(Minimum) < 0)
                {
                    defaultValue = Minimum;
                }
                else
                {
                    defaultValue = value;
                }
                ChangedDefaultValue?.Invoke(this, new EventArgs());
            }
        }

        public event EventHandler ChangedMinimum;

        public event EventHandler Minimized;

        public event EventHandler Changed;

        public event EventHandler Increased;

        public event EventHandler Decreased;

        public event EventHandler ChangedMaximum;

        public event EventHandler Maximized;

        public event EventHandler ChangedDefaultValue;

        public event EventHandler RestoredDefaultValue;

        public Characteristic(T minimum, T current, T maximum, T defaultValue)
        {
            Minimum = minimum;
            Maximum = maximum;
            Current = current;
            DefaultValue = defaultValue;
        }

        public void Decrease(T dicrement)
        {
            Current -= (dynamic)dicrement;
            Decreased?.Invoke(this, new EventArgs());
        }

        public void Increase(T increment)
        {
            Current += (dynamic)increment;
            Increased?.Invoke(this, new EventArgs());
        }

        public void ResetToMaximum()
        {
            Current = Maximum;
            Maximized?.Invoke(this, new EventArgs());
        }

        public void ResetToMinimum()
        {
            Current = Minimum;
            Minimized?.Invoke(this, new EventArgs());
        }

        public void RestoreToDefaultValue()
        {
            Current = DefaultValue;
            RestoredDefaultValue?.Invoke(this, new EventArgs());
        }

        public override string ToString()
        {
            return $"{Minimum}-{Current}-{Maximum}-{DefaultValue}\n";
        }
    }
}
