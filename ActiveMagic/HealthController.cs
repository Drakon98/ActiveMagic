﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public class HealthController
    {
        public Health Health { get; private set; }

        public int X { get; private set; } = 1;

        public HealthController()
        {
            Health = new Health(new Characteristic<uint>(100, 0, 0, 0));
            EventHandler activatedState = null;
            activatedState += ActivateState;
            var newState = new State("Die", new Range(0, 0), true, activatedState, activatedState);
            Health.AddState(newState);
            Console.WriteLine(Health.States[0]);
            Health.States[0].SetIncluded(false);
            Console.WriteLine(Health.States[0]);
            Health.ChangeState(newState);
            Console.WriteLine(Health.States[0]);
            Health.SelectState(newState);
            Health.ActivateState();
            Console.WriteLine(X);

            var health = new Characteristic<decimal>(0, 10, 20, 20);
            Console.WriteLine(health);
            health.Increase(20);
            Console.WriteLine(health);
            health.Decrease(50);
            Console.WriteLine(health);
            health.Decrease(-50);
            Console.WriteLine(health);
            health.Increase(-50);
            Console.WriteLine(health);
        }

        public void ActivateState(object sender, EventArgs eventArgs)
        {
            X *= 5;
        }
    }
}
