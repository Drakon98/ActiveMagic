﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public interface IChangeable : IList<State>
    {
        IReadOnlyList<State> States { get; }

        void Change(State state);
    }
}
