﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public class Carrying
    {
        public string Name { get; } = "Carrying";

        public decimal Current { get; private set; }

        public decimal Minimum { get; } = 0;

        public decimal Maximum { get; }

        public decimal DefaultValue => throw new NotImplementedException();

        public event EventHandler Changed;

        public event EventHandler Increased;

        public event EventHandler Decreased;
        public event EventHandler ChangedMinimum;
        public event EventHandler Minimized;
        public event EventHandler ChangedMaximum;
        public event EventHandler Maximized;
        public event EventHandler ChangedDefaultValue;
        public event EventHandler RestoredDefaultValue;

        public Carrying(decimal maximum, decimal current, decimal minimum, string name)
        {
            Maximum = maximum;
            Minimum = minimum;
            Current = current;
            Name = name;
        }

        public Carrying(decimal maximum, decimal current, decimal minimum)
        {
            Maximum = maximum;
            Minimum = minimum;
            Current = current;
        }

        public Carrying(decimal maximum, decimal current)
        {
            Maximum = maximum;
            Current = current;
        }

        public Carrying(decimal maximum)
        {
            Maximum = maximum;
            Current = maximum;
        }

        public void Decrease(decimal dicrement)
        {
            Current -= dicrement;
            Changed?.Invoke(this, new EventArgs());
            Decreased?.Invoke(this, new EventArgs());
        }

        public void Increase(decimal increment)
        {
            Current += increment;
            Changed?.Invoke(this, new EventArgs());
            Increased?.Invoke(this, new EventArgs());
        }

        public void Change(decimal newCurrent)
        {
            throw new NotImplementedException();
        }

        public void ChangeMinimum(decimal newMinimum)
        {
            throw new NotImplementedException();
        }

        public void ResetToMinimum()
        {
            throw new NotImplementedException();
        }

        public void ChangeMaximum(decimal newMaximum)
        {
            throw new NotImplementedException();
        }

        public void ResetToMaximum()
        {
            throw new NotImplementedException();
        }

        public void ChangeDefaultValue(decimal newDefault)
        {
            throw new NotImplementedException();
        }

        public void RestoreToDefaultValue()
        {
            throw new NotImplementedException();
        }
    }
}
