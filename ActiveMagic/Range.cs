﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public struct Range
    {
        public decimal Minimum { get; }

        public decimal Maximum { get; }

        public Range(decimal minimum, decimal maximum)
        {
            Minimum = minimum;
            Maximum = maximum;
        }

        public override string ToString()
        {
            return $"Диапазон: {Minimum}-{Maximum}\n";
        }
    }
}
