﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public class Speed
    {
        private float current;

        public string Name { get; } = "Speed";

        public float Current { get => current; private set { if (value > Maximum) current = Maximum; else if (value < Minimum) current = Minimum; else current = value; } }

        public float Minimum { get; } = 0;

        public float Maximum { get; }

        public float DefaultValue => throw new NotImplementedException();

        public event EventHandler Changed;

        public event EventHandler Increased;

        public event EventHandler Decreased;
        public event EventHandler ChangedMinimum;
        public event EventHandler Minimized;
        public event EventHandler ChangedMaximum;
        public event EventHandler Maximized;
        public event EventHandler ChangedDefaultValue;
        public event EventHandler RestoredDefaultValue;

        public Speed(int maximum, int current, int minimum, string name)
        {
            Maximum = maximum;
            Minimum = minimum;
            Current = current;
            Name = name;
        }

        public Speed(int maximum, int current, int minimum)
        {
            Maximum = maximum;
            Minimum = minimum;
            Current = current;
        }

        public Speed(int maximum, int current)
        {
            Maximum = maximum;
            Current = current;
        }

        public Speed(int maximum)
        {
            Maximum = maximum;
            Current = maximum;
        }

        public void Decrease(float dicrement)
        {
            Current -= dicrement;
            Changed?.Invoke(this, new EventArgs());
            Decreased?.Invoke(this, new EventArgs());
        }

        public void Increase(float increment)
        {
            Current += increment;
            Changed?.Invoke(this, new EventArgs());
            Increased?.Invoke(this, new EventArgs());
        }

        public void Change(float newCurrent)
        {
            throw new NotImplementedException();
        }

        public void ChangeMinimum(float newMinimum)
        {
            throw new NotImplementedException();
        }

        public void ResetToMinimum()
        {
            throw new NotImplementedException();
        }

        public void ChangeMaximum(float newMaximum)
        {
            throw new NotImplementedException();
        }

        public void ResetToMaximum()
        {
            throw new NotImplementedException();
        }

        public void ChangeDefaultValue(float newDefault)
        {
            throw new NotImplementedException();
        }

        public void RestoreToDefaultValue()
        {
            throw new NotImplementedException();
        }
    }
}
