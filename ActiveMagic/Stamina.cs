﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public class Stamina
    {
        private int current;

        public string Name { get; } = "Stamina";

        public int Current { get => current; private set { if (value > Maximum) current = Maximum; else if (value < Minimum) current = Minimum; else current = value; } }

        public int Minimum { get; } = 0;

        public int Maximum { get; }

        public int DefaultValue => throw new NotImplementedException();

        public event EventHandler Changed;

        public event EventHandler Increased;

        public event EventHandler Decreased;
        public event EventHandler ChangedMinimum;
        public event EventHandler Minimized;
        public event EventHandler ChangedMaximum;
        public event EventHandler Maximized;
        public event EventHandler ChangedDefaultValue;
        public event EventHandler RestoredDefaultValue;

        public Stamina(int maximum, int current, int minimum, string name)
        {
            Maximum = maximum;
            Minimum = minimum;
            Current = current;
            Name = name;
        }

        public Stamina(int maximum, int current, int minimum)
        {
            Maximum = maximum;
            Minimum = minimum;
            Current = current;
        }

        public Stamina(int maximum, int current)
        {
            Maximum = maximum;
            Current = current;
        }

        public Stamina(int maximum)
        {
            Maximum = maximum;
            Current = maximum;
        }

        public void Decrease(int dicrement)
        {
            Current -= dicrement;
            Changed?.Invoke(this, new EventArgs());
            Decreased?.Invoke(this, new EventArgs());
        }

        public void Increase(int increment)
        {
            Current += increment;
            Changed?.Invoke(this, new EventArgs());
            Increased?.Invoke(this, new EventArgs());
        }

        public void Change(int newCurrent)
        {
            throw new NotImplementedException();
        }

        public void ChangeMinimum(int newMinimum)
        {
            throw new NotImplementedException();
        }

        public void ResetToMinimum()
        {
            throw new NotImplementedException();
        }

        public void ChangeMaximum(int newMaximum)
        {
            throw new NotImplementedException();
        }

        public void ResetToMaximum()
        {
            throw new NotImplementedException();
        }

        public void ChangeDefaultValue(int newDefault)
        {
            throw new NotImplementedException();
        }

        public void RestoreToDefaultValue()
        {
            throw new NotImplementedException();
        }
    }
}
