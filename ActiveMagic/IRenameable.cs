﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public interface IRenameable : INameable
    {
        void Rename(string newName);
    }
}
