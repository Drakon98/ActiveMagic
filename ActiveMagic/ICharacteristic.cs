﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public interface ICharacteristic<T>
    {
        T Minimum { get; set; }

        T Current { get; set; }

        T Maximum { get; set; }

        T DefaultValue { get; set; }

        event EventHandler ChangedMinimum;

        event EventHandler Minimized;

        event EventHandler Changed;

        event EventHandler Increased;

        event EventHandler Decreased;

        event EventHandler ChangedMaximum;

        event EventHandler Maximized;

        event EventHandler ChangedDefaultValue;

        event EventHandler RestoredDefaultValue;

        void ResetToMinimum();

        void Increase(T increment);

        void Decrease(T dicrement);

        void ResetToMaximum();

        void RestoreToDefaultValue();
    }
}
