﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public class StateCollection : IChangeable
    {
        public State this[int index] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public IReadOnlyList<State> States => throw new NotImplementedException();

        public int Count => throw new NotImplementedException();

        public bool IsReadOnly => throw new NotImplementedException();

        public void Add(State state)
        {
            throw new NotImplementedException();
        }

        public void Change(State state)
        {
            throw new NotImplementedException();
        }

        public void ChangeState(State state)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(State item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(State[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<State> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public int IndexOf(State item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, State item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(State state)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
