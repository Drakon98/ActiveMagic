﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public class OverflowCharacteristic<T> : ICharacteristic<T> where T : struct, IComparable
    {
        private T minimum;

        private T current;

        private T maximum;

        private T defaultValue;

        public bool IsOverflowMinimum { get; }

        public bool IsOverflowMaximum { get; }

        public T Minimum 
        { 
            get => minimum; 
            
            set 
            {
                if (IsOverflowMinimum)
                {
                    minimum = value;
                    ChangedMinimum?.Invoke(this, new EventArgs());
                }
            } 
        }

        public T Current 
        { 
            get => current; 
            
            set
            {
                if (!IsOverflowMaximum && value.CompareTo(Maximum) > 0)
                {
                    current = Maximum;
                }
                else if (!IsOverflowMinimum && value.CompareTo(Minimum) < 0)
                {
                    current = Minimum;
                }
                else
                {
                    current = value;
                }
                Changed?.Invoke(this, new EventArgs());
            }
        }

        public T Maximum 
        { 
            get => maximum; 

            set 
            {
                if (IsOverflowMaximum)
                {
                    maximum = value;
                    ChangedMaximum?.Invoke(this, new EventArgs());
                }
            }
        }

        public T DefaultValue
        {
            get => defaultValue;

            set
            {
                if (!IsOverflowMaximum && value.CompareTo(Maximum) > 0)
                {
                    defaultValue = Maximum;
                }
                else if (!IsOverflowMinimum && value.CompareTo(Minimum) < 0)
                {
                    defaultValue = Minimum;
                }
                else
                {
                    defaultValue = value;
                }
                ChangedDefaultValue?.Invoke(this, new EventArgs());
            }
        }

        public event EventHandler ChangedMinimum;

        public event EventHandler Minimized;

        public event EventHandler Changed;

        public event EventHandler Increased;

        public event EventHandler Decreased;

        public event EventHandler ChangedMaximum;

        public event EventHandler Maximized;

        public event EventHandler ChangedDefaultValue;

        public event EventHandler RestoredDefaultValue;

        public OverflowCharacteristic(T minimum, T current, T maximum, T defaultValue, bool isOverflowMinimum = false, bool isOverflowMaximum = true)
        {
            Minimum = minimum;
            Maximum = maximum;
            Current = current;
            DefaultValue = defaultValue;
            IsOverflowMinimum = isOverflowMinimum;
            IsOverflowMaximum = isOverflowMaximum;
        }

        public void Decrease(T dicrement)
        {
            Current -= (dynamic)dicrement;
            Decreased?.Invoke(this, new EventArgs());
        }

        public void Increase(T increment)
        {
            Current += (dynamic)increment;
            Increased?.Invoke(this, new EventArgs());
        }

        public void ResetToMaximum()
        {
            Current = Maximum;
            Maximized?.Invoke(this, new EventArgs());
        }

        public void ResetToMinimum()
        {
            Current = Minimum;
            Minimized?.Invoke(this, new EventArgs());
        }

        public void RestoreToDefaultValue()
        {
            Current = DefaultValue;
            RestoredDefaultValue?.Invoke(this, new EventArgs());
        }

        public override string ToString()
        {
            return $"{Minimum}-{Current}-{Maximum}-{DefaultValue}-{IsOverflowMinimum}-{IsOverflowMaximum}\n";
        }
    }
}
