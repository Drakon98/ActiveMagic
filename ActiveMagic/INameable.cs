﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public interface INameable
    {
        string Name { get; }
    }
}
