﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActiveMagic
{
    public class Health : INameable//, IChangeable
    {
        private const uint defaultMaxHealth = 100;

        private const uint defaultCurrentHealth = 50;

        private const uint defaultMinHealth = 0;

        private const int defaultRegeneration = 0;

        private const string defaultName = "Health";

        public ICharacteristic<uint> Characteristic { get; private set; }

        public bool IsDied { get; private set; } = false;

        public string Name { get; private set; }

        private List<State> states = new List<State>();

        public IReadOnlyList<State> States { get => states; }

        public State CurrentState { get; private set; }

        public event EventHandler Died;

        public event EventHandler Ressurected;

        public Health(ICharacteristic<uint> characteristic)
        {
            Characteristic = characteristic;
        }

        public void Die()
        {
            if (Characteristic.Current == 0)
            {
                IsDied = true;
                Died?.Invoke(this, new EventArgs());
            }
        }

        public void Ressurect()
        {
            if(Characteristic.Current > 0 && IsDied == true)
            {
                IsDied = false;
                Ressurected?.Invoke(this, new EventArgs());
            }
        }

        public void AddState(State state)
        {
            states.Add(state);
        }

        public void SelectState(State state) => CurrentState = state;

        public void ActivateState() => CurrentState.ActivateState();

        public void DisactivateState() => CurrentState.DisactivateState();

        public void ChangeState(State state) => states[states.FindIndex(p => p.Name == state.Name)] = state;

        public void RenameState(string oldName, string newName) => states[states.FindIndex(p => p.Name == oldName)].Rename(newName);

        public void RemoveState(State state) => states.Remove(state);

    }
}
